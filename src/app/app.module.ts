import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// import "materialize-css";
import "angular2-materialize";

import { MaterializeModule } from 'angular2-materialize';

import { AppComponent } from './app.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { MainContentComponent } from './main-content/main-content.component';
import { ProjectComponent } from './project/project.component';
import { routing } from './app.routing';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BidderComponent } from './bidder/bidder.component';
import { ContractorComponent } from './contractor/contractor.component';
import { HistoryComponent } from './history/history.component';
import { MessageComponent } from './message/message.component';
import { NotificationComponent } from './notification/notification.component';
import { SettingsComponent } from './settings/settings.component';
import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
// import {StickyModule} from 'ng2-sticky-kit/ng2-sticky-kit';

// import { fakeBackendProvider } from './_helpers/index';
// import { MockBackend, MockConnection } from '@angular/http/testing';
// import { BaseRequestOptions } from '@angular/http';


import { AuthGuard } from './_guards/auth.guard';
import { AuthenticationService} from './_services/authentication.service';
import { UserService} from './_services/user.service';



@NgModule({
  declarations: [
    AppComponent,
    SideNavComponent,
    NavbarComponent,
    FooterComponent,
    MainContentComponent,
    ProjectComponent,
    DashboardComponent,
    BidderComponent,
    ContractorComponent,
    HistoryComponent,
    MessageComponent,
    NotificationComponent,
    SettingsComponent,
    ProfileComponent,
    LoginComponent,
    HomeComponent   
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterializeModule,
    routing
    // StickyModule
  ],
  providers: [
    AuthGuard,
    AuthenticationService,
    UserService,

    // providers used to create fake backend
    // fakeBackendProvider,
    // MockBackend,
    // BaseRequestOptions

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
