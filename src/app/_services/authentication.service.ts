﻿import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

@Injectable()
export class AuthenticationService {
    public token: string;

    constructor(private http: Http) {
        // set token if saved in local storage
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
    }

    login(username: string, password: string): Observable<boolean> {
        return this.http.post('/api/authenticate', JSON.stringify({ username: username, password: password }))
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let token = response.json() && response.json().token;
                if (token) {
                    // set token property
                    this.token = token;

                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify({ username: username, token: token }));

                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            });
    }

    login2(username: string, password: string): Observable<boolean> {
    // add authorization header with jwt token
        let headers = new Headers({ 'Authorization': 'Basic YnJvd3Nlcjo=' });
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        let options = new RequestOptions({ headers: headers });

        let body = new URLSearchParams();
        body.append('username', username);
        body.append('password', password);
        body.append('grant_type', 'password');

        let bodyString = body.toString()

        console.log("call this")


        return this.http.post('http://localhost:8080/oauth/token', bodyString, options)
            .map((response: Response) => {
                console.log("tesss");
                // login successful if there's a jwt token in the response
                let token = response.json() && response.json().access_token;
                // console.log(response.json());
                if (token) {
                    // set token property
                    this.token = token;

                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify({ username: username, token: token }));

                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            })
             .catch(err =>  { 
                // notify('UI error handling');
                return Observable.throw(err); // observable needs to be returned or exception raised
            });
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('currentUser');
    }
}