import { Routes, RouterModule } from '@angular/router';

import { ProjectComponent } from './project/project.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BidderComponent } from './bidder/bidder.component';
import { ContractorComponent } from './contractor/contractor.component';
import { HistoryComponent } from './history/history.component';
import { MessageComponent } from './message/message.component';
import { NotificationComponent } from './notification/notification.component';
import { SettingsComponent } from './settings/settings.component';
import { ProfileComponent } from './profile/profile.component';

import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';

import { AuthGuard } from './_guards/index';


const appRoutes: Routes = [
  { path: 'project', component: ProjectComponent, canActivate: [AuthGuard] },
  { path: 'bidder', component: BidderComponent , canActivate: [AuthGuard]},
  { path: 'contractor', component: ContractorComponent , canActivate: [AuthGuard]},
  { path: 'history', component: HistoryComponent , canActivate: [AuthGuard]},
  { path: 'message', component: MessageComponent , canActivate: [AuthGuard]},
  { path: 'notification', component: NotificationComponent , canActivate: [AuthGuard]},
  { path: 'settings', component: SettingsComponent , canActivate: [AuthGuard]},
  { path: 'profile', component: ProfileComponent , canActivate: [AuthGuard]},
  { path: 'dashboard', component: DashboardComponent , canActivate: [AuthGuard]},
  { path: '', pathMatch: 'full', redirectTo: 'dashboard' , canActivate: [AuthGuard]},

  { path: 'login', component: LoginComponent },
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);
