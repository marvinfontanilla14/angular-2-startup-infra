# Akona

Prerequisite
* Download and Install Node JS Latest Version
  https://nodejs.org/en/download/current/

  This will install Nodejs and npm

* Download and Install Angular CLI

https://github.com/angular/angular-cli

Check the above link for installation instruction

# Up and Run Akona

1. Go to Akona root folder
2. Enter 'ng serve' to run the server
3. Copy the url   http://localhost:4200/   and paste to your browser

You should be able to see akona web.