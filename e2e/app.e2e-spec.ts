import { AkonaPage } from './app.po';

describe('akona App', () => {
  let page: AkonaPage;

  beforeEach(() => {
    page = new AkonaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
